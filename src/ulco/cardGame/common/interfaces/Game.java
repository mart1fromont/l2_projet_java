package ulco.cardGame.common.interfaces;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.util.List;
import java.util.Map;

public interface Game extends Serializable {

    /**
     * Initialize the whole game using a parameter file
     * @param filename Game file
     */
    void initialize(String filename);

    Player run(Map<Player, Socket> playerSockets) throws IOException, ClassNotFoundException;

    /**
     * Add player to the current Game
     * @param player Player to add
     * @param socket
     */
    boolean addPlayer(Player player, Socket socket) throws Exception;

    /**
     * Remove player from the game using the reference
     * @param player Player to remove
     */
    void removePlayer(Player player);

    /**
     * Remove players from the game
     */
    void removePlayers();

    /**
     * Return player from current Game
     * @return List of all players
     */
    List<Player> getPlayers();

    /**
     * Specify if game is started or not
     * @return
     */
    boolean isStarted();

    /**
     * current number of players inside the Game
     * @return
     */
    Integer maxNumberOfPlayers();

    /**
     * Specify if the Game has end or not
     */
    boolean end();

    /**
     * Display current Game state
     * Such as user state (name and score)
     */
    void displayState();
}
