package ulco.cardGame.common.interfaces;

import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.util.List;

public interface Player extends Serializable {

    /**
     * Get name of the current Player
     * @return Player's name
     */
    String getName();

    /**
     * Get the score of the current Player
     * @return Player's score
     */
    Integer getScore();

    /**
     * Player do an action into a Game
     * @param socket
     */
    void play(Socket socket) throws IOException;

    /**
     * Depending of game rules, specify if the player can play currently
     * @return True if player is playing/can play, False otherwise
     */
    boolean isPlaying();

    /**
     * Specify if current user can play or not
     * @param playing Boolean to set playing value
     */
    void canPlay(boolean playing);

    /**
     * Add new component linked to Player
     */
    void addComponent(Component component);

    /**
     * Remove component from Player hand
     */
    void removeComponent(Component component);

    /**
     * Get all component in Player hand
     * @return list of components
     */
    List<Component> getComponents();

    /**
     * Get all specific component in Player hand
     * @return list of specific components
     */
    List<Component> getSpecificComponents(Class c);

    /**
     * Shuffle components hand of current Player
     */
    void shuffleHand();

    /**
     * Remove all components from Player hand
     */
    void clearHand();

    /**
     * Shows the current player's hand
     */
    void displayHand();
    String displayHandToString();
}
