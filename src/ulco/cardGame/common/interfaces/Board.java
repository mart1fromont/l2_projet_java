package ulco.cardGame.common.interfaces;

import ulco.cardGame.common.games.components.Component;

import java.io.Serializable;
import java.util.List;

public interface Board extends Serializable {

    /** Clears all the components in the board */
    void clear();

    /** Adds a specific component to the board */
    void addComponent(Component component);

    /** Gets a list of all the components in the board
     * @return List of board's components
     */
    List<Component> getComponents();

    /** Gets a list of all components in a specific class, that are in the board
     * @return List of board's components
     */
    List<Component> getSpecificComponents(Class c);

    /** Get the current state of the board */
    void displayState();

    String displayStateToString();
}
