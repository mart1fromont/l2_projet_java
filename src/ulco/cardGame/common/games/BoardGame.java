package ulco.cardGame.common.games;

import ulco.cardGame.common.boards.CardBoard;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public abstract class BoardGame implements Game {

    /** Variables */
    protected String name;
    protected Integer maxPlayers;
    protected List<Player> players = new ArrayList<>();

    protected boolean endGame;
    protected boolean started;
    protected Board board = new CardBoard();


    // Getters / Setters
    /** Get the game board
     * @return Current game board
     */
    public Board getBoard() { return this.board; }

    /** Get if the game has started or can start
     * @return True if game started/can start, False otherwise
     */
    public boolean isStarted() { return this.started; }

    /** Get the max number of players
     * @return Max Players authorized
     */
    public Integer maxNumberOfPlayers(){ return this.maxPlayers; }

    /** Get the list of current players
     * @return Current players' list
     */
    public List<Player> getPlayers(){ return this.players; }


    /** Constructor */
    public BoardGame(String name, Integer maxPLayers, String filename){
        this.name = name;
        this.maxPlayers = maxPLayers;
        this.started = this.endGame = false;
        this.initialize(filename);
    }


    // Functions
    /** Add a player to the player list
     * @param player Player to add
     * @return True if player has been added, False otherwise
     */
    public boolean addPlayer(Player player, Socket socket) throws Exception {
        // Check current names
        for (Player p : this.players)
            if (p.getName().equals(player.getName())){
                ObjectOutputStream playerOos = new ObjectOutputStream(socket.getOutputStream());
                playerOos.writeObject("\nLOBBY --> Your name is already in use, please choose another");

                ObjectOutputStream playerOos_b = new ObjectOutputStream(socket.getOutputStream());
                playerOos_b.writeObject("ERR_CHANGE_USERNAME");
                return false;
            }

        // Check capacity
        if (this.players.size() + 1 > this.maxNumberOfPlayers()){
            ObjectOutputStream playerOos = new ObjectOutputStream(socket.getOutputStream());
            playerOos.writeObject("\nLOBBY --> This server is already full. Try connecting later");

            ObjectOutputStream playerOos_b = new ObjectOutputStream(socket.getOutputStream());
            playerOos_b.writeObject("ERR_SERVER_FULL");
            return false;
        }

        // Add the new player and and send a start signal
        if (this.players.size() + 1 == this.maxNumberOfPlayers()){
            this.players.add(player);
            this.started = true;
            return true;

        // Add the new player but do not start
        } else {
            this.players.add(player);
            this.started = false;
            ObjectOutputStream playerOos = new ObjectOutputStream(socket.getOutputStream());
            playerOos.writeObject("\nLOBBY --> The server is waiting for others players to start...");
            return true;
        }
    }

    /** Removes a player from the current players' list
     * @param player Player to remove
     */
    public void removePlayer(Player player){ this.players.remove(player); }

    /**
     * Removes all the players from the current players' list
     */
    public void removePlayers(){ this.players.clear(); }

    /**
     * Displays a list of the current players' list
     */
    public void displayState(){
        System.out.println(this);
        System.out.println("---> Current players: ");
        int i = 1;
        for (Player p : this.players){
            System.out.println("\tPlayer " + i + ": " + p.toString());
            i++;
        }
    }
}
