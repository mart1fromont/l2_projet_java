package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.*;

public class BattleGame extends CardGame {

    // Constructor
    public BattleGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);
    }

    /** The main game loop, V2 with super-hyped battle
     * @return Player who won the game
     * @param playerSockets List of all players's sockets
     */
    public Player run(Map<Player, Socket> playerSockets) throws IOException, ClassNotFoundException {
        // Send the game type (for GUI)
        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("GAME_TYPE=BATTLE");
        }

        System.out.println("SERVER --> Game started");
        // Start the game with some informations
        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("\n·-----------------------------·\n" + "|   Now playing: Card game 2  |" +
                    "\n·-----------------------------·\n\n-> Players are:");
        }
        int i = 1;
        for (Player p : this.players){
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("\tPlayer " + i + ": " + p.getName());
            }
            p.canPlay(true); // Each player can play at start
            i++;
        }

        // Cleaning the board
        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("\n-> Cleaning the board...");
        }
        System.out.println("\n-> Cleaning the board...");
        this.getBoard().clear();

        // Shuffling cards before the game starts
        Collections.shuffle(this.cards);
        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("\n-> Cards shuffled ! Now distributing to players...");
        }
        System.out.println("\n-> Cards shuffled ! Now distributing to players...");

        // Distribute all cards to players
        int currentPlayer = 0;
        for (i = 0; i < this.cards.size(); ++i){
            this.getPlayers().get(currentPlayer).addComponent(this.cards.get(i));

            if (currentPlayer >= this.getPlayers().size() - 1)
                currentPlayer = 0;
            else
                currentPlayer++;
        }

        // Shows the cards players should have
        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject(this.cards.size() % this.getPlayers().size() == 0
                    ? "-> Cards distributed ! Each player should now have " + this.cards.size() / this.maxPlayers + " cards"
                    : "-> Cards distributed ! Each player should now have " + this.cards.size() / this.maxPlayers
                    + " or " + (this.cards.size() / this.maxPlayers + 1) + " cards");
        }
        System.out.println(this.cards.size() % this.getPlayers().size() == 0
                ? "-> Cards distributed ! Each player should now have " + this.cards.size() / this.maxPlayers + " cards"
                : "-> Cards distributed ! Each player should now have " + this.cards.size() / this.maxPlayers
                + " or " + (this.cards.size() / this.maxPlayers + 1) + " cards");

        // GUI update
        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("GUI_UPDATE");

            ObjectOutputStream playerOos_b = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos_b.writeObject(this);
        }

        // make the server wait for clients
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // GUI update to start game
        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("GUI_START_GAME");

            ObjectOutputStream playerOos_b = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos_b.writeObject(this);
        }

        // Main game loop
        while (!this.end()){ // while no player won
            this.numberOfRounds++;
            if (this.numberOfRounds % 10 == 0) { // Shuffle round
                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("\n-> Time to shuffle ! All cards have been shuffled");
                }
                System.out.println("\n-> Time to shuffle ! All cards have been shuffled");
                for (Player p : this.getPlayers())
                    p.shuffleHand();
            }

            // GUI update for new round
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("GUI_UPDATE");

                ObjectOutputStream playerOos_b = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos_b.writeObject(this);
            }

            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("-> New round (round " + this.numberOfRounds + "): put your cards !");
            }
            System.out.println("\n-> New round (round " + this.numberOfRounds + "): put your cards !");

            // Store player's played cards
            Map<Card, Player> playedCards = new HashMap<>();

            // Put cards
            for (Player p : this.getPlayers()){
                if (!p.isPlaying() || p.getScore() <= 0) { // if player can't play, skip him
                    for (Socket playerSocket : playerSockets.values()) {
                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        if (playerSockets.get(p) != playerSocket)
                            playerOos.writeObject(p.getName() + " cannot play");
                        else
                            playerOos.writeObject("You cannot play");

                    }
                    continue;
                }

                for (Socket playerSocket : playerSockets.values()) {
                    if (playerSockets.get(p) != playerSocket) {
                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOos.writeObject("Waiting for " + p.getName() + " to play...");
                    }else{
                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOos.writeObject("[" + p.getName() + "]" + " It's your turn !");

                        ObjectOutputStream playerOos_b = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOos_b.writeObject(this);
                    }
                }

                // read socket Data
                ObjectInputStream ois = new ObjectInputStream(playerSockets.get(p).getInputStream());
                Card card = (Card) ois.readObject();

                playedCards.put(card, p);
                this.getBoard().addComponent(card);
                p.removeComponent(card);

                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    if (playerSockets.get(p) != playerSocket) {
                        if (p.isPlaying())
                            playerOos.writeObject(p.getName() + " played the " + card.toString());
                        else // if it was the last card of the player #suspense
                            playerOos.writeObject(p.getName() + " played his last card, the " + card.toString());
                    }else{
                        playerOos.writeObject("You played the " + card.toString());
                    }
                }

            }

            // Check for highest value
            // this will store winner(s) of the round
            Map<Player, Card> winnersList = new LinkedHashMap<>(); // this will store winner(s) of the round
            int maxValue = -1;
            for (Map.Entry<Card ,Player> set : playedCards.entrySet()) {
                if (winnersList.size() == 0 || set.getKey().getValue() > maxValue) { // greater value has been found
                    winnersList.clear();
                    winnersList.put(set.getValue(), set.getKey());
                    maxValue = set.getKey().getValue();
                } else if (set.getKey().getValue() == maxValue) // multiple players have the same card value
                    winnersList.put(set.getValue(), set.getKey());
            }

            // Check for winner and get one randomly if multiple winners
            Map<Player, Card> battleCards = new HashMap<>(); // used only, if there's a battle
            while (winnersList.size() != 1) { // there are multiple winners for the round
                battleCards.clear();

                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("-> New round (round " + this.numberOfRounds + "): put your cards !");
                }
                System.out.println("\n-> Battle ! " + winnersList.size() + " players have the same card value !");

                // Start a battle in the rules of art
                // Put a hidden card for each player
                for (Map.Entry<Player, Card> set : winnersList.entrySet()){
                    if (set.getKey().isPlaying()){ // if player can play, put one of his cards
                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSockets.get(set.getKey()).getOutputStream());
                        playerOos.writeObject("[" + set.getKey().getName() + "]" + " It's your turn ! Put face down");

                        ObjectOutputStream playerOos_b = new ObjectOutputStream(playerSockets.get(set.getKey()).getOutputStream());
                        playerOos_b.writeObject(this);

                        ObjectInputStream ois = new ObjectInputStream(playerSockets.get(set.getKey()).getInputStream());
                        Card card = (Card) ois.readObject();
                        card.setHidden(true);
                        playedCards.put(card, set.getKey());
                        set.getKey().removeComponent(card);
                        this.getBoard().addComponent(card);

                    }else{ // if player can't play, choose one randomly
                        int maxWait = 0;
                        Player p;
                        do {
                            // no player have a card to play (*2 to be sure)
                            p = maxWait > winnersList.size() * 2
                                    ? (Player) this.getPlayers().toArray()
                                    [new Random().nextInt(this.getPlayers().size())]
                                    : (Player) winnersList.keySet().toArray()
                                    [new Random().nextInt(winnersList.size())];
                           maxWait++;
                        } while (!p.isPlaying());

                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSockets.get(p).getOutputStream());
                        playerOos.writeObject("[" + p.getName() + "]" + " help face down");

                        ObjectOutputStream playerOos_p = new ObjectOutputStream(playerSockets.get(p).getOutputStream());
                        playerOos_p.writeObject(this);

                        ObjectInputStream ois = new ObjectInputStream(playerSockets.get(p).getInputStream());
                        Card card = (Card) ois.readObject();
                        card.setHidden(true);
                        playedCards.put(card, set.getKey());
                        p.removeComponent(card);
                        this.getBoard().addComponent(card);

                        for (Socket playerSocket : playerSockets.values()) {
                            ObjectOutputStream playerOos_b = new ObjectOutputStream(playerSocket.getOutputStream());
                            if (playerSockets.get(set.getKey()) != playerSocket)
                                playerOos_b.writeObject(set.getKey().getName() + " cannot play, and took a card from "
                                + p.getName());
                            else
                                playerOos_b.writeObject("You can't play, and took a card from " + p.getName());
                        }
                        System.out.println(set.getKey().getName() + " cannot play, and took a card from "
                                + p.getName());
                    }

                    for (Socket playerSocket : playerSockets.values()) {
                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        if (playerSockets.get(set.getKey()) != playerSocket)
                            playerOos.writeObject(set.getKey().getName() + " played a card face down");
                        else
                            playerOos.writeObject("You played a card face down");
                    }

                    System.out.println(set.getKey().getName() + " played a card face down");
                }

                // Play a card
                for (Map.Entry<Player, Card> set : winnersList.entrySet()){
                    if (set.getKey().isPlaying()) { // if player can play, put one of his cards
                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSockets.get(set.getKey()).getOutputStream());
                        playerOos.writeObject("[" + set.getKey().getName() + "]" + " It's your turn !");

                        ObjectOutputStream playerOos_p = new ObjectOutputStream(playerSockets.get(set.getKey()).getOutputStream());
                        playerOos_p.writeObject(this);

                        ObjectInputStream ois = new ObjectInputStream(playerSockets.get(set.getKey()).getInputStream());
                        Card card = (Card) ois.readObject();
                        playedCards.put(card, set.getKey());
                        battleCards.put(set.getKey(), card);
                        this.getBoard().addComponent(card);
                        set.getKey().removeComponent(card);

                        for (Socket playerSocket : playerSockets.values()) {
                            ObjectOutputStream playerOos_b = new ObjectOutputStream(playerSocket.getOutputStream());
                            if (playerSockets.get(set.getKey()) != playerSocket) {
                                if (set.getKey().isPlaying())
                                    playerOos_b.writeObject(set.getKey().getName() + " played the " + card.toString());
                                else // if it was the last card of the player #suspense
                                    playerOos_b.writeObject(set.getKey().getName() + " played his last card, the "
                                            + card.toString());
                            }else{
                                playerOos_b.writeObject("You played the " + card.toString());
                            }
                        }

                    }else{ // if player can't play, choose one randomly
                        int maxWait = 0;
                        Player p;
                        do {
                            // no player have a card to play (*2 to be sure)
                            p = maxWait > winnersList.size() * 2
                                    ? (Player) this.getPlayers().toArray()
                                    [new Random().nextInt(this.getPlayers().size())]
                                    : (Player) winnersList.keySet().toArray()
                                    [new Random().nextInt(winnersList.size())];
                            maxWait++;
                        } while (!p.isPlaying());

                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSockets.get(p).getOutputStream());
                        playerOos.writeObject("[" + p.getName() + "]" + " help play");

                        ObjectOutputStream playerOos_p = new ObjectOutputStream(playerSockets.get(p).getOutputStream());
                        playerOos_p.writeObject(this);

                        ObjectInputStream ois = new ObjectInputStream(playerSockets.get(p).getInputStream());
                        Card card = (Card) ois.readObject();
                        playedCards.put(card, set.getKey());
                        battleCards.put(set.getKey(), card);
                        p.removeComponent(card);
                        this.getBoard().addComponent(card);

                        for (Socket playerSocket : playerSockets.values()) {
                            ObjectOutputStream playerOos_b = new ObjectOutputStream(playerSocket.getOutputStream());
                            if (playerSockets.get(set.getKey()) != playerSocket)
                                playerOos_b.writeObject(set.getKey().getName() + " cannot play, and took a card from "
                                        + p.getName() + "\nHe played the " + card);
                            else
                                playerOos_b.writeObject("You can't play, and took a card from " + p.getName()
                                + "\nYou played the " + card);
                        }
                        System.out.println(set.getKey().getName() + " cannot play, and took a card from "
                                + p.getName());
                        System.out.println(set.getKey().getName() + " played the " + card.toString());
                    }
                }

                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("-> Checking your cards... *trumpets*");
                }
                System.out.println("-> Checking your cards... *trumpets*");
                System.out.print("\n");

                // Check for highest value
                winnersList.clear(); // clear old winners, replace by new
                int maxCard = -1;
                for (Map.Entry<Player ,Card> set : battleCards.entrySet()) {
                    if (winnersList.size() == 0 || set.getValue().getValue() > maxCard) { // greater value has been found
                        winnersList.clear();
                        winnersList.put(set.getKey(), set.getValue());
                        maxCard = set.getValue().getValue();
                    } else if (set.getValue().getValue() == maxCard) // multiple players have the same card value
                        winnersList.put(set.getKey(), set.getValue());
                }
            }

            // Get the winner
            Player winner = (Player) winnersList.keySet().toArray()[0]; // winner is, by default, the first key

            // Shows and clear the board
            this.getBoard().displayState();
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject(this.getBoard().displayStateToString());
            }
            this.getBoard().clear();

            // Show the round winner on the screen
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("\n-> " + winner.getName() + " won the round and took " + (playedCards.size()
                        + battleCards.size() - 1) + (playedCards.size() + battleCards.size() == 2 ? " card !" : " cards !"));
            }
            System.out.println("\n-> " + winner.getName() + " won the round and took " + (playedCards.size()
                    + battleCards.size() - 1) + (playedCards.size() + battleCards.size() == 2 ? " card !" : " cards !"));

            // Give played cards to the winner
            for (Map.Entry<Card ,Player> set : playedCards.entrySet())
                winner.addComponent(set.getKey());
        }

        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("\n·----------------------·\n|   Card game ended !  |\n·----------------------·\n");
        }
        System.out.println("\n·----------------------·\n|   Card game ended !  |\n·----------------------·\n");

        // Get and return the winner
        for (Player p : this.getPlayers())
            if (p.getScore() == 52)
                return p;

        // This should never happens. Never.
        return null;
    }
}
