package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.interfaces.Player;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.util.*;

public class CardGame extends BoardGame {

    /** Variables */
    protected List<Card> cards;
    protected Integer numberOfRounds;


    // Constructor
    public CardGame(String name, Integer maxPlayers, String filename){
        super(name, maxPlayers, filename);
        this.numberOfRounds = 0;
    }


    // Functions
    /** Initialize the card game and load the specified cards file
     * @param filename Cards file
     */
    public void initialize(String filename){
        this.cards = new ArrayList<>();
        try {
            File cardFile = new File(filename);
            Scanner reader = new Scanner(cardFile);
            while (reader.hasNextLine()){
                String[] cardInfo = reader.nextLine().split(";");
                Card card = new Card(cardInfo[0], Integer.parseInt( cardInfo[1]));
                this.cards.add(card);
            }
            reader.close();
        }catch (FileNotFoundException e){ // This should not happens
            System.out.println("--->" + this.name + ": Cannot read cards file");
            e.printStackTrace();
        }
    }

    /** The main game loop
     * @return Player who won the game
     * @param playerSockets
     */
    public Player run(Map<Player, Socket> playerSockets) throws IOException, ClassNotFoundException {
        // Start the game with some informations
        System.out.println("\n·-----------------------------·\n|   Now playing: Card game !  |" +
                "\n·-----------------------------·\n\n-> Players are:");
        int i = 1;
        for (Player p : this.players){
            System.out.println("\tPlayer " + i + ": " + p.getName());
            p.canPlay(true); // Each player can play at start
            i++;
        }

        // Cleaning the board
        System.out.println("\n-> Cleaning the board...");
        this.getBoard().clear();

        // Shuffling cards before the game starts
        Collections.shuffle(this.cards);
        System.out.println("\n-> Cards shuffled ! Now distributing to players...");

        // Distribute all cards to players
        int currentPlayer = 0;
        for (i = 0; i < this.cards.size(); ++i){
            this.getPlayers().get(currentPlayer).addComponent(this.cards.get(i));

            if (currentPlayer >= this.getPlayers().size() - 1)
                currentPlayer = 0;
            else
                currentPlayer++;
        }

        // Shows the cards players should have
        System.out.println(this.cards.size() % this.getPlayers().size() == 0
                ? "-> Cards distributed ! Each player should now have " + this.cards.size() / this.maxPlayers + " cards"
                : "-> Cards distributed ! Each player should now have " + this.cards.size() / this.maxPlayers
                + " or " + (this.cards.size() / this.maxPlayers + 1) + " cards");

        // Main game loop
        while (!this.end()){ // while no player won
            this.numberOfRounds++;
            if (this.numberOfRounds % 10 == 0) { // Shuffle round
                System.out.println("\n-> Time to shuffle ! All cards have been shuffled");
                for (Player p : this.getPlayers())
                    p.shuffleHand();
            }

            System.out.println("\n-> New round (round " + this.numberOfRounds + "): put your cards !");

            // Store player's played cards
            Map<Player, Card> playedCards = new HashMap<>();

            // Put cards
            for (Player p : this.getPlayers()){
                if (!p.isPlaying()) // if player can't play, skip him
                    continue;

                //Card card = (Card) p.play(playerSockets.get(p));
                //playedCards.put(p, card);
                //this.getBoard().addComponent(card);

                //if (p.isPlaying())
                //    System.out.println(p.getName() + " played the " + card.toString());
                //else // if it was the last card of the player #suspense
                //    System.out.println(p.getName() + " played his last card, the " + card.toString());
            }

            // Check for highest value
            Map<Player, Card> winnersList = new LinkedHashMap<>(); // this will store winner(s) of the round
            int maxValue = -1;
            for (Map.Entry<Player ,Card> set : playedCards.entrySet()) {
                if (winnersList.size() == 0 || set.getValue().getValue() > maxValue) { // greater value has been found
                    winnersList.clear();
                    winnersList.put(set.getKey(), set.getValue());
                    maxValue = set.getValue().getValue();
                } else if (set.getValue().getValue() == maxValue) // multiple players have the same card value
                    winnersList.put(set.getKey(), set.getValue());
            }

            // Check for winner and get one randomly if multiple winners
            Player winner = (Player) winnersList.keySet().toArray()[0]; // winner is, by default, the first key
            if (winnersList.size() != 1) { // there are multiple winners for the round
                System.out.println("-> Battle ! " + winnersList.size() + " players have the same card value !");
                winner = (Player) winnersList.keySet().toArray()[new Random().nextInt(winnersList.size())];
                System.out.println("The very fair computer chosen \"" + winner.getName() + "\"");
            }

            // Shows and clear the board
            this.getBoard().displayState();
            this.getBoard().clear();

            // Show the round winner on the screen
            System.out.println("-> " + winner.getName() + " won the round and took " + (playedCards.size() - 1)
                    + (playedCards.size() == 2 ? " card !" : " cards !"));

            // Give played cards to the winner
            for (Map.Entry<Player ,Card> set : playedCards.entrySet())
                winner.addComponent(set.getValue());
        }

        System.out.println("\n·----------------------·\n|   Card game ended !  |\n·----------------------·\n");

        // Get and return the winner
        for (Player p : this.getPlayers())
            if (p.getScore() == 52)
                return p;

        // This should never happens. Never.
        return null;
    }

    /** Check if the game is finished, and check if there is a winner
     * @return False if no winner or if game is not finished, True otherwise
     */
    public boolean end(){
        for (Player p : this.getPlayers())
            if (p.getScore() == 52)
                return true;

        return false;
    }

    @Override
    public String toString() { return "---> Game info:\n\tName: " + this.name; }
}
