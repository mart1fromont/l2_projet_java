package ulco.cardGame.common.games;

import ulco.cardGame.common.boards.PokerBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Chip;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class PokerGame extends BoardGame {

    /** Variables */
    protected List<Card> cards;
    protected List<Chip> chips;
    protected final int maxRounds;
    protected int numberOfRounds;

    /** Constructor */
    public PokerGame(String name, Integer maxPLayers, String filename, int maxRounds) {
        super(name, maxPLayers, filename);
        this.maxRounds = maxRounds;
        this.board = new PokerBoard();
    }

    /** Initialize the poker game and load the specified poker file
     * @param filename Cards file
     */
    public void initialize(String filename){
        this.cards = new ArrayList<>();
        this.chips = new ArrayList<>();
        try {
            File pokerFile = new File(filename);
            Scanner reader = new Scanner(pokerFile);

            while (reader.hasNextLine()){
                String[] pokerInfo = reader.nextLine().split(";");

                if (pokerInfo[0].equals("Card")){
                    Card card = new Card(pokerInfo[1], Integer.parseInt(pokerInfo[2]));
                    this.cards.add(card);
                }else{
                    Chip chip = new Chip(pokerInfo[1], Integer.parseInt(pokerInfo[2]));
                    this.chips.add(chip);
                }
            }

            reader.close();
        }catch (FileNotFoundException e){ // This should not happens
            System.out.println("--->" + this.name + ": Cannot read poker file");
            e.printStackTrace();
        }
    }

    @Override
    public Player run(Map<Player, Socket> playerSockets) throws IOException, ClassNotFoundException {
        // Start the game with some informations
        System.out.println("\n·------------------------------·\n|  Now playing: Poker game !   |" +
                "\n·------------------------------·\n\n-> Players are:");
        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("\n·------------------------------·\n|  Now playing: Poker game !   |" +
                    "\n·------------------------------·\n\n-> Players are:");
        }
        int j = 1;
        for (Player p : this.players){
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("\tPlayer " + j + ": " + p.getName());
            }
            p.canPlay(true); // Each player can play at start
            j++;
        }

        // Get sum of all chips
        int sumOfChips = this.chips.stream().mapToInt(Component::getValue).sum();

        // Give chips to players
        System.out.println("\n-> Giving coins to players (each player should have " + sumOfChips + " points," +
                " and " + this.chips.size() + " coins)");
        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("\n-> Giving coins to players (each player should have " + sumOfChips + " points," +
                    " and " + this.chips.size() + " coins)");
        }

        for (Player p : this.getPlayers())
            for (Chip s : this.chips)
                p.addComponent(s);

        this.numberOfRounds = 1;
        while (!this.end()){
            // get the list of players that are playing
            List<Player> playingPlayers = new ArrayList<>(this.getPlayers());

            // reset board and hands
            this.getBoard().clear();
            for (Player p : this.getPlayers()){
                p.clearHand();
            }


            int cardsHead = 0; // will be used to know which card is on top of cards list
            System.out.println("\n-> Now playing Round " + this.numberOfRounds + " !");
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("\n-> Now playing Round " + this.numberOfRounds + " !");
            }

            // Shuffle cards and distribute to players
            Collections.shuffle(this.cards);
            System.out.println("-> Shuffling cards ! *swoosh*");
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("-> Shuffling cards ! *swoosh*");
            }

            int currentPlayer = 0;
            for (int i = 0; i < this.getPlayers().size() * 3; ++i){
                this.getPlayers().get(currentPlayer).addComponent(this.cards.get(cardsHead));
                cardsHead++;

                if (currentPlayer >= this.getPlayers().size() - 1)
                    currentPlayer = 0;
                else
                    currentPlayer++;
            }

            System.out.println("-> Given 3 cards to players");
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("-> Given 3 cards to players");
            }

            // Bet a chip from each player, if he can
            for (Player p : this.getPlayers()){
                if (p.isPlaying()){ // if player can play, bet a chip
                    if (!playingPlayers.contains(p) || playingPlayers.size() <= 1)
                        continue;

                    System.out.println("\n-> Time to bet for " + p.getName() + " !");
                    for (Socket playerSocket : playerSockets.values()) {
                        if (playerSockets.get(p) != playerSocket) {
                            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                            playerOos.writeObject("\n-> Time to bet for " + p.getName() + " !");
                        }else{
                            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                            playerOos.writeObject("[" + p.getName() + "]" + " It's your turn !");

                            ObjectOutputStream playerOos_b = new ObjectOutputStream(playerSocket.getOutputStream());
                            playerOos_b.writeObject(this);
                        }
                    }
                    p.displayHand();

                    ObjectInputStream ois = new ObjectInputStream(playerSockets.get(p).getInputStream());
                    Chip playedChip = (Chip) ois.readObject();

                    if (playedChip != null) {
                        this.getBoard().addComponent(playedChip);
                        p.removeComponent(playedChip);
                        for (Socket playerSocket : playerSockets.values())
                            if (playerSockets.get(p) != playerSocket) {
                                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                playerOos.writeObject(p.getName() + ": bet a " + playedChip.getName() + " coin !");
                            }

                    }else{ // player fold
                        playingPlayers.remove(p);
                        System.out.println(p.getName() + ": fold !");
                        for (Socket playerSocket : playerSockets.values())
                            if (playerSockets.get(p) != playerSocket) {
                                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                playerOos.writeObject(p.getName() + ": fold !");
                            } else {
                                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                playerOos.writeObject("You fold !");
                            }
                    }
                }else {
                    System.out.println(p.getName() + " can't bet !");
                    for (Socket playerSocket : playerSockets.values())
                        if (playerSockets.get(p) != playerSocket) {
                            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                            playerOos.writeObject(p.getName() + " can't bet !");
                        } else {
                            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                            playerOos.writeObject("You can't bet !");
                        }
                }
            }

            // Place 3 cards to the board
            System.out.println("-> Placed 3 cards to the board");
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("-> Placed 3 cards to the board");
            }

            if (playingPlayers.size() > 1){
                for (int i = 0; i < 3; ++i){
                    this.getBoard().addComponent(this.cards.get(cardsHead));
                    cardsHead++;
                }
            }

            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject(this.getBoard().displayStateToString());
            }
            this.getBoard().displayState();

            System.out.print("\n-> Now bet again !");
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("-> Now bet again !");
            }

            // Bet a chip from each player, if he can
            if (playingPlayers.size() > 1){
                for (Player p : this.getPlayers()){
                    if (!playingPlayers.contains(p) || playingPlayers.size() <= 1)
                        continue;

                    if (p.isPlaying()){ // if player can play, bet a chip
                        for (Socket playerSocket : playerSockets.values()) {
                            if (playerSockets.get(p) != playerSocket) {
                                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                playerOos.writeObject("\n-> Time to bet for " + p.getName() + " !");
                            }else{
                                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                playerOos.writeObject("[" + p.getName() + "]" + " It's your turn !");

                                ObjectOutputStream playerOos_b = new ObjectOutputStream(playerSocket.getOutputStream());
                                playerOos_b.writeObject(this);
                            }
                        }
                        p.displayHand();
                        ObjectInputStream ois = new ObjectInputStream(playerSockets.get(p).getInputStream());
                        Chip playedChip = (Chip) ois.readObject();

                        if (playedChip != null) {
                            this.getBoard().addComponent(playedChip);
                            p.removeComponent(playedChip);
                            for (Socket playerSocket : playerSockets.values())
                                if (playerSockets.get(p) != playerSocket) {
                                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                    playerOos.writeObject(p.getName() + ": bet a " + playedChip.getName() + " coin !");
                                }

                        }else{ // player fold
                            playingPlayers.remove(p);
                            System.out.println(p.getName() + ": fold !");
                            for (Socket playerSocket : playerSockets.values())
                                if (playerSockets.get(p) != playerSocket) {
                                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                    playerOos.writeObject(p.getName() + ": fold !");
                                } else {
                                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                    playerOos.writeObject("You fold !");
                                }
                        }
                    }else {
                        System.out.println(p.getName() + " can't bet !");
                        for (Socket playerSocket : playerSockets.values())
                            if (playerSockets.get(p) != playerSocket) {
                                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                playerOos.writeObject(p.getName() + " can't bet !");
                            } else {
                                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                playerOos.writeObject("You can't bet !");
                            }
                    }
                }
            }

            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject(this.getBoard().displayStateToString());
            }
            this.getBoard().displayState();

            // Show the cards for each player and check for winner(s)
            Map<Player, Integer> maxDuplicateCard = new HashMap<>();
            Map<Player, Integer> maxDuplicates = new HashMap<>();

            System.out.print("\n-> Bets finished ! Show your cards !");
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("\n-> Bets finished ! Show your cards !");
            }
            for (Player p : this.getPlayers()){
                if (!playingPlayers.contains(p))
                    continue;

                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject(p.getName() + "'s hand:\n" + p.displayHandToString());
                }
                p.displayHand();

                // Calculate duplicates
                List<Component> allCards = new ArrayList<>();
                allCards.addAll(p.getSpecificComponents(Card.class));
                allCards.addAll(this.getBoard().getSpecificComponents(Card.class));

                // Count the duplicates in the board + the hand
                Set<Integer> duplicates = new LinkedHashSet<>();
                Set<Integer> uniques = new HashSet<>();
                for(Component t : allCards)
                    if(!uniques.add(t.getValue())){
                        duplicates.add(t.getValue());
                    }

                // Get the highest duplicated value
                int max = -1;
                for (Integer entry : duplicates)
                    if (entry > max)
                        max = entry;

                maxDuplicateCard.put(p, max);
                maxDuplicates.put(p, duplicates.size());
            }

            // Get the list of players with max duplicates
            Map<Player, Integer> winnersList = new LinkedHashMap<>();
            int maxPairs = -1;
            int maxValue = -1;

            for (Map.Entry<Player ,Integer> set : maxDuplicates.entrySet()) {
                // if greater number of duplicates than current winner
                if (maxPairs == -1 || set.getValue() > maxPairs){
                    maxPairs = set.getValue();
                    maxValue = maxDuplicateCard.get(set.getKey());
                    winnersList.clear();
                    winnersList.put(set.getKey(), set.getValue());

                }else if (set.getValue() == maxPairs){ // if same number of duplicates than current winner
                    if (maxDuplicateCard.get(set.getKey()) == maxValue) { // if also same max value
                        winnersList.put(set.getKey(), set.getValue());

                    }else if (maxDuplicateCard.get(set.getKey()) > maxValue){ // if max value greater
                        maxPairs = set.getValue();
                        maxValue = maxDuplicateCard.get(set.getKey());
                        winnersList.clear();
                        winnersList.put(set.getKey(), set.getValue());
                    }
                }
            }

            if (winnersList.size() > 1){ // there's multiple winners, oh no

                int numberOfChips = 0, totalValue = 0;
                for (Component chip : this.getBoard().getSpecificComponents(Chip.class)){
                    Player p = (Player) winnersList.keySet().toArray()[new Random().nextInt(winnersList.size())];
                    p.addComponent(chip);
                    totalValue += chip.getValue();
                    numberOfChips++;
                }

                System.out.println("\n-> " + winnersList.size() + " players won the round ! They gained "
                        + numberOfChips + " coins to divide, for a total value of " + totalValue + " points");
                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("\n-> " + winnersList.size() + " players won the round ! They gained "
                            + numberOfChips + " coins to divide, for a total value of " + totalValue + " points");
                }
                System.out.println();

            }else{ // there's one winner, we did it
                Player winner = (Player) winnersList.keySet().toArray()[0];

                int numberOfChips = 0, totalValue = 0;
                for (Component chip : this.getBoard().getSpecificComponents(Chip.class)){
                    winner.addComponent(chip);
                    totalValue += chip.getValue();
                    numberOfChips++;
                }

                System.out.println("\n-> " + winner.getName() + " won the round ! He gained " + numberOfChips
                        + " coins, for a total value of " + totalValue + " points\n-> " + winner.getName()
                        + " is now at " + winner.getScore() + " points !");
                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("\n-> " + winner.getName() + " won the round ! He gained " + numberOfChips
                            + " coins, for a total value of " + totalValue + " points\n-> " + winner.getName()
                            + " is now at " + winner.getScore() + " points !");
                }
            }

            System.out.println("-> Next round in 5 seconds... be ready !");
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("-> Next round in 5 seconds... be ready !");
            }
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }

            this.numberOfRounds++;
        }

        Player gameWinner = null;
        int maxVal = -1;

        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("\n·----------------------·\n|  Poker game ended !  |\n·----------------------·\n");
        }
        System.out.println("\n·----------------------·\n|  Poker game ended !  |\n·----------------------·\n");

        for (Player p : this.getPlayers()){
            if (maxVal == -1 || p.getScore() > maxVal) {
                gameWinner = p;
                maxVal = p.getScore();
            }
        }

        return gameWinner;
    }

    @Override
    public boolean end() {
        return this.numberOfRounds > this.maxRounds;
    }
}
