package ulco.cardGame.common.games.components;

public class Chip extends Component {
    public Chip(String name, Integer value) {
        super(name, value);
    }

    @Override
    public String toString() {
        return this.getName() + " coin (" + this.getValue() + ")";
    }
}
