package ulco.cardGame.common.games.components;

public class Card extends Component {

    /** Variables */
    private boolean hidden;


    // Getters / Setters
    public boolean isHidden() { return hidden; }

    public void setHidden(boolean hidden) { this.hidden = hidden; }


    // Constructor
    public Card(String name, int value) {
        super(name, value);
    }

    @Override
    public String toString() {
        String retval = "";

        switch (this.name.substring(1)){
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
            case "10":
                retval += this.name.substring(1);
                break;
            case "J":
                retval += "Jack";
                break;
            case "Q":
                retval += "Queen";
                break;
            case "K":
                retval += "King";
                break;
            case "A":
                retval += "Ace";
                break;
        }

        retval += " of ";
        switch (this.name.charAt(0)){
            case 'S':
                retval += "Spades";
                break;
            case 'H':
                retval += "Hearts";
                break;
            case 'C':
                retval += "Clubs";
                break;
            case 'D':
                retval += "Diamonds";
                break;
        }

        return retval;
    }
}
