package ulco.cardGame.common.games.players;

import ulco.cardGame.common.interfaces.Player;

public abstract class BoardPlayer implements Player {

    /** Variables */
    protected String name;
    protected boolean playing;
    protected Integer score;


    // Getters / Setters
    /** Set the current player's playing status
     * @param playing Boolean representing the status
     */
    public void canPlay(boolean playing) { this.playing = playing; }

    public String getName() { return name; }

    public boolean isPlaying() { return playing; }


    // Constructor
    public BoardPlayer(String name) {
        this.name = name;
        this.playing = false;
        this.score = 0;
    }


    // Functions
    @Override
    public String toString() {
        return "---> Player Info\n\tName: " + this.getName()
                + "\n\tScore: " + this.getScore()
                + "\n\tStatus: " + (this.isPlaying() ? "Playing" : "Not playing");
    }
}
