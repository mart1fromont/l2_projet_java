package ulco.cardGame.common.games.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardPlayer extends BoardPlayer {

    /** Variables */
    private final List<Card> cards;


    // Getters / Setters
    public Integer getScore() { return this.score; }

    // Constructor
    public CardPlayer(String name){
        super(name);
        this.cards = new ArrayList<>();
    }


    // Functions
    /** Adds a card to the player's cards
     * @param component Card to add
     */
    public void addComponent(Component component){
        this.cards.add((Card) component);
        this.score++;
        this.canPlay(true);
    }

    /** Removes a card from the player's cards list
     * @param component Card to remove
     */
    public void removeComponent(Component component){
        this.cards.removeIf(card -> card.getId().equals(component.getId()));
        this.score--;
        if (this.score <= 0) this.canPlay(false);
    }

    /** Plays the first player card from his hand and removes it
     * @param socket Player's socket
     */
    public void play(Socket socket) throws IOException {
        Card retval = this.cards.get(0);
        this.removeComponent(this.cards.get(0));

        // Send card to server
        ObjectOutputStream playerOos = new ObjectOutputStream(socket.getOutputStream());
        playerOos.writeObject(retval);
    }

    /** Get the player's cards hand
     * @return Current player's cards hand
     */
    public List<Component> getComponents(){ return new ArrayList<>(this.cards); }

    /** Get the player's cards hand
     * @return Current player's cards hand
     */
    public List<Component> getSpecificComponents(Class c){ return new ArrayList<>(this.cards); }


    /**
     * Shuffles the current players' cards list
     */
    public void shuffleHand(){ Collections.shuffle(this.cards); }

    /**
     * Clears the current players' cards list
     */
    public void clearHand(){
        this.cards.clear();
        this.score = 0;
        this.canPlay(false);
    }

    @Override
    public void displayHand() {
        System.out.println("\n-> " + getName() + "'s hand");
        for (Component card : this.getComponents())
            System.out.println("\t- " + card.toString());
    }

    @Override
    public String displayHandToString() {
        return null;
    }

    @Override
    public String toString() {
        return "Card Player \"" + this.getName() + "\" (score of " + this.getScore() + ")";
    }
}
