package ulco.cardGame.common.games.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Chip;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.*;

public class PokerPlayer extends BoardPlayer {

    /** Variables */
    private final List<Card> cards;
    private final List<Chip> chips;


    // Getters / Setters
    public Integer getScore() { return this.score; }

    // Constructor
    public PokerPlayer(String name){
        super(name);
        this.cards = new ArrayList<>();
        this.chips = new ArrayList<>();
    }


    // Functions
    /** Adds a card/chip to the player's cards
     * @param component Card/chip to add
     */
    public void addComponent(Component component){
        if (component instanceof Card)
            this.cards.add((Card) component);
        else{
            this.chips.add((Chip) component);
            if (component != null) this.score += component.getValue();
        }

        this.canPlay(true);
    }

    /** Removes a card/chip from the player's cards list
     * @param component Card/chip to remove
     */
    public void removeComponent(Component component){
        if (component instanceof Card)
            this.cards.remove((Card) component);
        else{
            this.chips.remove((Chip) component);
            if (component != null) this.score -= component.getValue();
        }

        if (this.score <= 0) this.canPlay(false);
    }

    /** Plays the first player chip from his hand and removes it
     * @param socket Player's socket
     */
    public void play(Socket socket) throws IOException {
        // display player's hand
        System.out.println("--> Your hand:");
        this.displayHand();

        // Player should enter a valid color
        Chip retval = null;
        while (retval == null){
            System.out.println("\n" + this.getName() + " : Please enter a valid coin color you have (\"fold\" to fold)");
            Scanner reader = new Scanner(System.in);
            String color = reader.nextLine().toLowerCase();

            for (Chip s : this.chips){
                if (s.getName().toLowerCase().equals(color)){
                    retval = s;
                }
            }

            if (color.equals("fold")){
                // Send card to server
                ObjectOutputStream playerOos = new ObjectOutputStream(socket.getOutputStream());
                playerOos.writeObject(retval);
                return;
            }
        }

        // Send card to server
        ObjectOutputStream playerOos = new ObjectOutputStream(socket.getOutputStream());
        playerOos.writeObject(retval);
    }

    /** Get the player's cards hand / chips
     * @return Current player's cards hand / chips
     */
    public List<Component> getComponents(){
        List<Component> retval = new ArrayList<>();
        retval.addAll(this.cards);
        retval.addAll(this.chips);
        return retval;
    }

    /** Get the player's cards hand
     * @return Current player's cards hand
     */
    public List<Component> getSpecificComponents(Class c){
        if (c == Card.class) // cards required
            return new ArrayList<>(this.cards);
        else // else, return chips
            return new ArrayList<>(this.chips);
    }


    /**
     * Shuffles the current players' cards list
     */
    public void shuffleHand(){ Collections.shuffle(this.cards); }

    /**
     * Clears the current players' cards list
     */
    public void clearHand(){
        this.cards.clear();
        this.canPlay(false);
    }

    @Override
    public void displayHand() {
        // cards
        System.out.println("-> Cards");
        for (Component card : this.getSpecificComponents(Card.class))
            System.out.println("\t- " + card.toString());

        // chips
        Map<String, Integer> chips = new HashMap<>();
        System.out.println("-> Coins (" + this.score + ")");
        for (Component chip : this.getSpecificComponents(Chip.class)){
            if (!chips.containsKey(chip.getName()))
                chips.put(chip.getName(), 1);
            else{
                chips.put(chip.getName(), chips.get(chip.getName()) + 1);
            }
        }

        for (Map.Entry<String, Integer> entry : chips.entrySet())
            System.out.println(" - " + entry.getKey() + " coin x" + entry.getValue());
    }

    public String displayHandToString(){
        // cards
        StringBuilder retval = new StringBuilder("-> Cards");
        for (Component card : this.getSpecificComponents(Card.class))
            retval.append("\n\t- ").append(card.toString());

        // chips
        Map<String, Integer> chips = new HashMap<>();
        retval.append("\n-> Coins (").append(this.score).append(")");
        for (Component chip : this.getSpecificComponents(Chip.class)){
            if (!chips.containsKey(chip.getName()))
                chips.put(chip.getName(), 1);
            else{
                chips.put(chip.getName(), chips.get(chip.getName()) + 1);
            }
        }

        for (Map.Entry<String, Integer> entry : chips.entrySet())
            retval.append("\n - ").append(entry.getKey()).append(" coin x").append(entry.getValue());

        return retval.toString();
    }

    @Override
    public String toString() {
        return "Poker Player \"" + this.getName() + "\" (score of " + this.getScore() + ")";
    }

}
