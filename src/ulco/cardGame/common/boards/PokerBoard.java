package ulco.cardGame.common.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Chip;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PokerBoard implements Board {

    /** Variables */
    private List<Card> cards = new ArrayList<>();
    private List<Chip> chips = new ArrayList<>();

    /** Clears all the cards and chips in the board */
    @Override
    public void clear() {
        this.cards = new ArrayList<>();
        this.chips = new ArrayList<>();
    }

    /** Adds a specific card or chip to the board */
    @Override
    public void addComponent(Component component) {
        if (component instanceof Card) // if object is a card
            this.cards.add((Card) component);
        else // otherwise, it's a chip.
            this.chips.add((Chip) component);
    }

    /** Gets a list of all the cards and chips in the board
     * @return List of board's cards and chips
     */
    @Override
    public List<Component> getComponents() {
        List<Component> retval = new ArrayList<>();
        retval.addAll(this.cards);
        retval.addAll(this.chips);
        return retval;
    }

    /** Gets a list of all components in a specific class, that are in the board
     * @return List of board's components
     */
    @Override
    public List<Component> getSpecificComponents(Class c) {
        if (c == Card.class) // cards required
            return new ArrayList<>(this.cards);
        else // else, return chips
            return new ArrayList<>(this.chips);
    }

    /** Get the current state of the board (cards and chips) */
    @Override
    public void displayState() {
        System.out.println("\n--> Game board :");
        System.out.println("\n-> Central cards :");
        for (Component card : this.getSpecificComponents(Card.class))
            System.out.println("\t- " + card.toString());

        // chips
        Map<String, Integer> chips = new HashMap<>();
        System.out.println("-> Coins wagered :");
        for (Component chip : this.getSpecificComponents(Chip.class)){
            if (!chips.containsKey(chip.getName()))
                chips.put(chip.getName(), 1);
            else{
                chips.put(chip.getName(), chips.get(chip.getName()) + 1);
            }
        }

        for (Map.Entry<String, Integer> entry : chips.entrySet())
            System.out.println(" - " + entry.getKey() + " coin x" + entry.getValue());
    }

    @Override
    public String displayStateToString(){
        StringBuilder retval = new StringBuilder("\n--> Game board :\n-> Central cards :");
        for (Component card : this.getSpecificComponents(Card.class))
            retval.append("\n\t- ").append(card.toString());

        // chips
        Map<String, Integer> chips = new HashMap<>();
        retval.append("\n-> Coins wagered :");
        for (Component chip : this.getSpecificComponents(Chip.class)){
            if (!chips.containsKey(chip.getName()))
                chips.put(chip.getName(), 1);
            else{
                chips.put(chip.getName(), chips.get(chip.getName()) + 1);
            }
        }

        for (Map.Entry<String, Integer> entry : chips.entrySet())
            retval.append("\n - ").append(entry.getKey()).append(" coin x").append(entry.getValue());

        return retval.toString();
    }
}
