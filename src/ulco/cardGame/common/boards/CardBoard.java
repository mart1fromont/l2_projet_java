package ulco.cardGame.common.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class CardBoard implements Board {

   /** Variables */
   private List<Card> cards = new ArrayList<>();

    /** Clears all the cards in the board */
    @Override
    public void clear() { this.cards = new ArrayList<>(); }

    /** Adds a specific card to the board */
    @Override
    public void addComponent(Component component) { this.cards.add((Card) component); }

    /** Gets a list of all the cards in the board
     * @return List of board's cards
     */
    @Override
    public List<Component> getComponents() { return new ArrayList<>(this.cards); }

    /** Gets a list of all components in a specific class, that are in the board
     * @return List of board's components
     */
    @Override
    public List<Component> getSpecificComponents(Class c) { return this.getComponents(); }

    /** Get the current state of the board */
    @Override
    public void displayState() {
        System.out.println("\n--> Game board :");
        for (Component card : this.getComponents()){
            System.out.println("\t- " + card.toString());
        }
    }

    public String displayStateToString(){
        StringBuilder retval = new StringBuilder("\n--> Game board :");
        for (Component card : this.getComponents())
            retval.append("\n\t- ").append(card.toString());
        return retval.toString();
    }
}
