package ulco.cardGame.client;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.util.Duration;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.util.Objects;

public class ClientApplication extends Application {

    private Scene scene;

    public static void main(String[] args) {
        launch(args);
    }

    private void askMessage() {
        do {
            switch (Client.SERVER_CURRENT_ANSWER){
                case "GUI_UPDATE":
                    Client.SERVER_CURRENT_ANSWER = "none";
                    Platform.runLater(() -> updateGame(Client.game));
                    break;

                case "GUI_START_GAME":
                    Client.SERVER_CURRENT_ANSWER = "none";
                    Platform.runLater(() -> startGame(Client.game));
                    break;

                case "CARD":
                    Client.SERVER_CURRENT_ANSWER = "none";
                    Platform.runLater(this::showCard);
                    break;
            }

        } while (!Client.SERVER_CURRENT_ANSWER.equals("END"));
    }


    @Override
    public void start(Stage primaryStage) throws IOException {
        // Variables
        Stage stage = new Stage();

        if (Client.GAME_TYPE.equals("BATTLE")){
            Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("FXML/client.fxml")));
            scene = new Scene(root, 1000, 800);
            stage.setResizable(false);

            stage.setTitle("Battle Game");
            stage.setScene(scene);
            stage.show();
        }

        new Thread(this::askMessage).start();
    }

    /** Update the GUI */
    public void updateGame(Game game){
        int i = 0;
        for (Player p : game.getPlayers()){
            ((Label)scene.lookup("#player-" + i + "-name")).setText(!p.getName().equals(Client.username)
                    ? p.getName() : p.getName() + " (You)");
            ((Label)scene.lookup("#player-" + i + "-score")).setText("Score: " + p.getScore());

            if (!p.isPlaying() && game.isStarted()) {
                scene.lookup("#player-" + i + "-panel").setStyle("-fx-background-color: grey");
                ((Label) scene.lookup("#player-" + i + "-score")).setText("You don't have cards left !");
                scene.lookup("#player-" + i + "-score").setStyle("-fx-text-fill: white; -fx-font-size: 16px");
            }
            ++i;
        }
    }

    /** Plays an animation to start game */
    public void startGame(Game game){
        updateGame(game);
        showMessageGUI("GO !");
    }

    /** Shows a message in the GUI (apparently, this does not work.) */
    public void showMessageGUI(String message){
        // fade in
        ((Label)scene.lookup("#board-message")).setText(message);
        FadeTransition ft = new FadeTransition();
        ft.setToValue(100);
        ft.setDuration(Duration.millis(150));
        ft.setNode(scene.lookup("#board-message"));
        ft.play();

        // wait
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // fade out
        ((Label)scene.lookup("#board-message")).setText("GO !");
        FadeTransition ft2 = new FadeTransition();
        ft2.setToValue(0);
        ft2.setDuration(Duration.millis(150));
        ft2.setNode(scene.lookup("#board-message"));
        ft2.play();
    }

    /** Shows a card on GUI */
    public void showCard(){

    }
}
