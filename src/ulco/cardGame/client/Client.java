package ulco.cardGame.client;

import javafx.application.Application;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.server.SocketServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Client {

    // Variables
    public static String GAME_TYPE;
    protected static ClientApplication clientApp;
    protected static Game game;
    protected static String SERVER_CURRENT_ANSWER = "none";
    protected static String username;

    public static void main(String[] args) {

        // Current use full variables
        try {
            // Create a connection to the server socket on the server application
            InetAddress host = InetAddress.getLocalHost();
            Socket socket;

            Object answer;
            int playerNumber = -1;

            Scanner scanner = new Scanner(System.in);

            System.out.print("--> Please select your username: ");
            username = scanner.nextLine();

            socket = new Socket(host.getHostName(), SocketServer.PORT);

            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(username);

            if (username.startsWith("Patrick"))
                System.out.println("\n*Remember: every Patrick is a cheater*\n");

            do {
                // Read and display the response message sent by server application
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                answer = ois.readObject();

                if (answer instanceof Integer && playerNumber == -1) // set the player number in the game
                    playerNumber = (int) answer;

                // depending of object type, we can manage its data
                if (answer instanceof String){
                    SERVER_CURRENT_ANSWER = (String)answer;

                    if (answer.equals("END"))
                        continue;

                    else if (((String) answer).startsWith("GAME_TYPE")){ // server requested to launch GUI
                        GAME_TYPE = ((String) answer).split("=")[1];
                        clientApp = new ClientApplication();
                        new Thread(() -> Application.launch(clientApp.getClass())).start();
                    }

                    else if (answer.equals("GUI_UPDATE") || answer.equals("GUI_START_GAME")){
                        // wait for the game copy to be send
                        ObjectInputStream ois_game = new ObjectInputStream(socket.getInputStream());
                        game = (Game) ois_game.readObject();
                    }

                    else if (answer.equals("CHOOSE_GAME")){ // player should choose the server game
                        int gameName;

                        do {
                            System.out.print("\n--> Please choose a game to play:\n" +
                                    "\t0. Battle Game (with experimental GUI)\n\t1. Poker Game\n-> ");
                            gameName = scanner.nextInt();
                        }while(gameName < 0 || gameName > 1);

                        ObjectOutputStream oos_username = new ObjectOutputStream(socket.getOutputStream());
                        oos_username.writeObject(gameName);
                    }

                    else if (answer.equals("ERR_CHANGE_USERNAME")){ // error message to change username
                        System.out.print("\n--> Please change your username: ");
                        username = scanner.nextLine();

                        // reset the connection with the server
                        socket = new Socket(host.getHostName(), SocketServer.PORT);
                        ObjectOutputStream oos_username = new ObjectOutputStream(socket.getOutputStream());
                        oos_username.writeObject(username);
                    }

                    else if (answer.equals("ERR_SERVER_FULL")){ // error message if server is full
                        break;

                    }else if (((String) answer).startsWith("[" + username + "]")){ // it's the player's turn to play
                        if (((String) answer).contains("help"))
                            System.out.println("You have to help a player because he cannot play ! " +
                                    "Sending game informations...");
                        else
                            System.out.println("It's your turn ! Sending game informations...");

                        // wait for the game copy to be send
                        ObjectInputStream ois_game = new ObjectInputStream(socket.getInputStream());
                        game = (Game) ois_game.readObject();

                        System.out.println("Choose a card to play:");

                        // wait to simulate player thinking ...
                        try {
                            TimeUnit.SECONDS.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        // play and send informations to server
                        game.getPlayers().get(playerNumber).play(socket);
                    }

                    else
                        System.out.println(answer);
                }
                if (answer instanceof Game) {
                    ((Game) answer).displayState();
                    Client.game = (Game) answer;
                }

                if (answer instanceof Board)
                    ((Board) answer).displayState();

            } while (!answer.equals("END"));

            // check the winner
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            answer = ois.readObject();
            System.out.println(answer);

            // close the socket instance connection
            System.out.println("\nThanks for playing ! See you one day ...");
            socket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
