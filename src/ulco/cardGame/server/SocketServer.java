package ulco.cardGame.server;

import ulco.cardGame.common.games.BattleGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.games.players.CardPlayer;
import ulco.cardGame.common.games.players.PokerPlayer;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Server Game management with use of Singleton instance creation
 */
public class SocketServer {

    private ServerSocket server;
    protected Game game; // game will be instantiate later
    protected Map<Player, Socket> playerSockets;
    protected Constructor playerConstructor;

    public static final int PORT = 7777;

    private SocketServer() {
        try {
            server = new ServerSocket(PORT);
            playerSockets = new HashMap<>();
            playerConstructor = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SocketServer server = new SocketServer();
        server.run();
    }

    private void run() {
        try {

            // Need to specify the game for the first user
            Socket socket_first = server.accept();

            // read socket Data
            ObjectInputStream ois_first = new ObjectInputStream(socket_first.getInputStream());
            String playerUsername_first = (String) ois_first.readObject();
            Player firstPlayer = new CardPlayer(playerUsername_first); // by default, new player is card player

            // Choose the game (battle or poker)
            ObjectOutputStream oos_c_first = new ObjectOutputStream(socket_first.getOutputStream());
            oos_c_first.writeObject("CHOOSE_GAME");

            // Wait for the answer
            ObjectInputStream ois_game = new ObjectInputStream(socket_first.getInputStream());
            int answer = (int) ois_game.readObject();

            switch (answer){
                case 0: // battle chosen
                    game = new BattleGame("Battle's World Championship Series", 4,
                            "resources/games/cardGame.txt");
                    break;
                case 1:
                    game = new PokerGame("Las Vegas Poker World Championships", 4,
                            "resources/games/pokerGame.txt", 3);
                    firstPlayer = new PokerPlayer(playerUsername_first);
                    break;
            }

            // Store player's socket in dictionary (Map)
            playerSockets.put(firstPlayer, socket_first);

            // Tell the player his position in the player's list
            ObjectOutputStream oos_b_first = new ObjectOutputStream(socket_first.getOutputStream());
            oos_b_first.writeObject(0);
            System.out.println("SERVER --> " + firstPlayer.getName() + " joined the lobby");

            game.addPlayer(firstPlayer, socket_first);

            // Game Loop
            System.out.println("LOBBY --> Waiting for new players:\n");

            // add each player until game is ready (or number of max expected player is reached)
            // Waiting for the socket entrance
            do {

                Socket socket = server.accept();

                // read socket Data
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                String playerUsername = (String) ois.readObject();

                // Create player instance
                Player player;
                if (game instanceof PokerGame)
                    player = new PokerPlayer(playerUsername);
                else
                    player = new CardPlayer(playerUsername);

                if (game.addPlayer(player, socket)) {
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(game);

                    // Tell to other players that new player is connected
                    for (Socket playerSocket : playerSockets.values()) {
                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOos.writeObject("\nLOBBY --> " + player.getName() + " joined the lobby");
                    }

                    // Store player's socket in dictionary (Map)
                    playerSockets.put(player, socket);

                    // Tell the player his position in the player's list
                    ObjectOutputStream oos_b = new ObjectOutputStream(socket.getOutputStream());
                    oos_b.writeObject(game.getPlayers().size() - 1);

                    System.out.println("SERVER --> " + player.getName() + " joined the lobby");
                }
            } while (!game.isStarted());

            // run the whole game using sockets
            Player gameWinner = game.run(playerSockets);

            // Tells to player that server will be closed (just as example)
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("END");

                // the end of the war, gg wp guys
                ObjectOutputStream playerOos_end = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos_end.writeObject("-> Winner is " + gameWinner.getName() + " ! Congratulations !");
            }

            // Close each socket when game is finished
            for (Socket socket : playerSockets.values()) {
                socket.close();
            }
            game.removePlayers();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

