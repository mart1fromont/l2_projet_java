package ulco.cardGame.server;

import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.games.players.PokerPlayer;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

/**
 * Server Game management with use of Singleton instance creation
 */
public class GameServer {

    public static void main(String[] args) {
        /* TODO
        // - Create Game Instance
        // - Add players to game
        // - Run the Game loop
        // - Display the winner player
        Game game = new BattleGame(" Battle ", 3, "resources/games/cardGame.txt");

        Player player1 = new CardPlayer("Xx_darkUser1_xX");
        Player player2 = new CardPlayer("Xx_darkUser2_xX");
        Player player3 = new CardPlayer("Xx_darkUser3_xX");

        game.addPlayer(player1);
        game.addPlayer(player2);
        game.addPlayer(player3);

        Player winner = game.run();
        System.out.println("-> Winner is " + winner.getName() + " ! Congratulations !");

        //Game game = new PokerGame(" Poker ", 3, "resources/games/pokerGame.txt", 3);

        //Player player1 = new PokerPlayer("Jason Mercier");
        //Player player2 = new PokerPlayer("Michael Mizrachi");
        //Player player3 = new PokerPlayer("Patrick Bruel");

        //game.addPlayer(player1, socket);
        //game.addPlayer(player2, socket);
        //game.addPlayer(player3, socket);

        //Player winner = game.run(playerSockets);
        //System.out.println("-> Winner is " + player3.getName() + " ! Congratulations !");*/
    }
}

